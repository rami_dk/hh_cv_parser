import logging
from django.core.mail import EmailMultiAlternatives, EmailMessage

from django.conf import settings


class EmailBackend(object):
    """Бэкенд для отправки Ответа на email"""
    def send_email(self, subject, message, to_: (list, tuple), from_=None, email_type='text', attachment=()):
        """Отправляет email по заданными параметрам"""
        msg_kwargs = {}
        msg_kwargs['subject'] = subject
        msg_kwargs['from_email'] = from_ or settings.DEFAULT_FROM_EMAIL
        msg_kwargs['to'] = to_

        log_str = 'Subject: {subject}, From: {from_}, To: {to}'.format(subject=msg_kwargs['subject'],
                                                                       from_=msg_kwargs['from_email'],
                                                                       to=msg_kwargs['to'])

        if email_type == 'html':
            msg = EmailMultiAlternatives(**msg_kwargs)
            msg.attach_alternative(message, "text/html")
        elif email_type == 'text':
            msg = EmailMessage(**msg_kwargs)
            msg.body = message
        else:
            raise NotImplementedError('Email format "%s" not supported' % email_type)

        for file in attachment:
            msg.attach_file(file)

        try:
            msg.send(fail_silently=False)
        except Exception:
            logging.exception('Error sending email: %s' % log_str)
            raise
        else:
            logging.info('Email successfully sent: %s' % log_str)

    def send_data(self, response):
        """Принимает объект-наследник BaseResponse и отправляет email с его данными."""
        message = response.message()
        self.send_email(subject=response.subject, message=message, to_=response.receivers,
                        email_type=response.content_type, attachment=response.attachment)
