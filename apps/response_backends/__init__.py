import abc
import datetime
import inspect
import traceback


class BaseResponse(metaclass=abc.ABCMeta):
    """Абстрактный базовый класс Ответа"""
    CONTENT_TYPE_TEXT = 'text'
    CONTENT_TYPE_HTML = 'html'

    content_type = CONTENT_TYPE_TEXT  # типо содержимого Ответа
    settings = None

    @abc.abstractmethod
    def get_response_data(self, *args, **kwargs):
        """Формирование содержимого Ответа"""
        pass

    @property
    @abc.abstractmethod
    def subject(self) -> str:
        """Заголовок"""
        pass

    def message(self, *args, **kwargs) -> str:
        """Сообщение"""
        text = self.header + self.text(*args, *kwargs) + self.footer
        return text

    @property
    def header(self):
        """Заголовок Сообщения"""
        return 'Здравствуйте!\n'

    @abc.abstractmethod
    def text(self, *args, **kwargs) -> str:
        """Основное содержимое Сообщения.
        Форматирование содежимого Ответа."""
        pass

    @property
    def footer(self):
        """Подпись Сообщения"""
        return '\nС уважением,\nРобот-помощник'

    @property
    def attachment(self):
        """Вложения"""
        return []

    @property
    def receivers(self):
        """Список получателей данного Ответа"""
        return [x.strip() for x in self.settings.email_receivers.split(',')]


class BaseDebugResponse(BaseResponse):
    """Базовый класс Ответа для отладки - с трейсбэком, локальными переменными, бэкендом(опционально)
    Возвращается в случае сбоя."""
    def __init__(self, error, backend=None, *args, **kwargs):
        self.get_response_data(error, backend)

    def get_response_data(self, error, backend):
        self.error = error
        self.backend = backend
        self.traceback = traceback.format_exc()
        self.variables = inspect.trace()[-1][0].f_locals

    def text(self):
        variables = '\n'.join(['{}: {}'.format(key, repr(value)) for key, value in self.variables.items()])
        context = [repr(self.error), self.traceback, variables]

        if self.backend:
            context = [self.backend.__name__, ] + context

        text = '\n\n'.join(context)
        return text

    @property
    def receivers(self):
        return [x.strip() for x in self.settings.debug_email_receivers.split(',')]
