from django.apps import AppConfig


class ResumeParserAppConfig(AppConfig):
    name = 'apps.resume_parser.resume_parser_app'
    verbose_name = 'Парсер резюме'
