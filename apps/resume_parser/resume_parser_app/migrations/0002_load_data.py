# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-11-17 10:15
from __future__ import unicode_literals

import os

from django.core.management import call_command
from django.db import migrations

from django.conf import settings


def init_data(apps, schema_editor):
    call_command('loaddata', os.path.join(settings.BASE_DIR, 'apps', 'resume_parser', 'resume_parser_app', 'fixtures', 'initial_data.json'))


def backwards(*args, **kwargs):
    pass


class Migration(migrations.Migration):

    dependencies = [
        ('resume_parser_app', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(init_data, backwards)
    ]
