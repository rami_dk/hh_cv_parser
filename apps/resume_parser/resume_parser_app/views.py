import os

from django.http import HttpResponse, Http404

# Create your views here.
from django.views import View
from django.conf import settings


class ResumeDownloadView(View):
    """
    View для скачивания объединенного файла резюме.
    """
    def get(self, request, **kwargs):
        file_uuid = kwargs.get('file_uuid')

        try:
            file_names = os.listdir(settings.RESUME_PARSER_SETTINGS['MERGED_FILES_PATH'])
        except FileNotFoundError:
            raise Http404('Директория объединенных резюме не найдена')

        file_by_uuid = [x for x in file_names if x.startswith(file_uuid)]

        if not file_by_uuid:
            raise Http404

        file_by_uuid = file_by_uuid[0]

        with open(os.path.join(settings.RESUME_PARSER_SETTINGS['MERGED_FILES_PATH'], file_by_uuid), 'rb') as file:
            ext = file_by_uuid.split('.')[-1]

            response = HttpResponse(file.read(), content_type='application/pdf')
            response['Content-Disposition'] = "{attachment}filename={filename}".format(
                filename=file_by_uuid.split('%s_' % file_uuid)[-1],
                ext=ext,
                attachment='attachment; '
            )

        return response

