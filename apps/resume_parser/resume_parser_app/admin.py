from django.contrib import admin

# Register your models here.
from solo.admin import SingletonModelAdmin

from apps.resume_parser.resume_parser_app.models import Configuration

admin.site.register(Configuration, SingletonModelAdmin)