from django.conf.urls import url

from apps.resume_parser.resume_parser_app.views import ResumeDownloadView

urlpatterns = [
    url(r'merged_resumes/(?P<file_uuid>[0-9a-z\-]+)$',
        ResumeDownloadView.as_view(), name='merged_resumes'),
]
