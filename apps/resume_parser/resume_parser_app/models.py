from django.conf import settings
from django.db import models

# Create your models here.
from solo.models import SingletonModel


class Configuration(SingletonModel):
    MOSCOW = 1

    SEARCH_AREA_CHOICES = (
        (MOSCOW, 'Москва'),
    )

    email_receivers = models.CharField(max_length=255, default='info@example.com',
                                       help_text='Список получателей рассылки через запятую',
                                       verbose_name='Получатели рассылки')
    debug_email_receivers = models.CharField(max_length=255, default='info@example.com',
                                             help_text='Список получателей уведомлений при сбое'
                                                       ' (с детальной информацией для отладки) через запятую',
                                             verbose_name='Получатели сервисной рассылки')

    login = models.CharField('Логин', max_length=255, blank=True)
    password = models.CharField('Пароль', max_length=255, blank=True)

    key_words = models.TextField('Список ключевых фраз', help_text='Каждая ключевая фраза должны быть с новой строки', blank=True)
    search_area = models.SmallIntegerField('Регион поиска', choices=SEARCH_AREA_CHOICES, default=MOSCOW)

    saved_search_max_pages = models.SmallIntegerField('Макс. количество страниц в сохраненном поиске', default=100)

    @property
    def get_key_words(self):
        QUERY_CODE = settings.RESUME_PARSER_SETTINGS['STRICT_MATCH_CODE']
        return ['{}{}'.format(QUERY_CODE, x.lower().strip()) for x in self.key_words.split('\n')]

    def __str__(self):
        return 'Настройки'

    class Meta:
        verbose_name = 'Настройки'
        verbose_name_plural = 'Настройки'