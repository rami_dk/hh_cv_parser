import logging
import shutil

from django.conf import settings
from django.core.management.base import BaseCommand

from apps.response_backends.email_backend import EmailBackend
from apps.resume_parser.responses import ResumeResponse, DebugResponse, ErrorResponse
from apps.resume_parser.resume_parser_app.page_objects.utils import download_resumes

FILES_DOWNLOAD_PATH = settings.RESUME_PARSER_SETTINGS['FILES_DOWNLOAD_PATH']

class Command(BaseCommand):
    """
    * keywords = ключевые словам, указанные в настройках
    Команда парсит резюме с сайта hh.ru в формете PDF по keywords.
    Объединяет полученные резюме в один файл и отправляет на почту.

    По keywords создает сохраненные поиски в личном кабинете на сайте hh.ru, т.е.
    если keywords изменился, то данные изменения отслеживаются и учитываются.

    Работает не быстро.
    """
    help = 'Parse resumes from hh.ru, merge to one file and send it to email.'

    def handle(self, *args, **kwargs):
        logging.info('Command started')
        try:
            # Удаляем директорию со скачанными резюме (если существует)
            logging.info('Deleting resumes download dir if exists: %s' % FILES_DOWNLOAD_PATH)
            shutil.rmtree(FILES_DOWNLOAD_PATH, ignore_errors=True)

            download_resumes()
            EmailBackend().send_data(ResumeResponse())
        except Exception as e:
                logging.exception('Error parsing resumes')
                EmailBackend().send_data(DebugResponse(e))
                EmailBackend().send_data(ErrorResponse())
        else:
            logging.info('Success parsing resumes')
        finally:
            # Удаляем директорию со скачанными резюме (если существует)
            logging.info('Deleting resumes download dir: %s' % FILES_DOWNLOAD_PATH)
            shutil.rmtree(FILES_DOWNLOAD_PATH, ignore_errors=True)
            logging.info('Command finished')




