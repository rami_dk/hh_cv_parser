import logging
import time

import os
from django.conf import settings
import selene.config
from selene.conditions import exist
from selene.support import by
from selene.support.jquery_style_selectors import ss, s
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.keys import Keys

from apps.resume_parser.resume_parser_app.models import Configuration
from apps.resume_parser.resume_parser_app.page_objects.decorators import cached
from apps.resume_parser.resume_parser_app.selene_custom.utils import open_url

BASE_URL = selene.config.app_host if selene.config.app_host else selene.config.base_url
QUERY_CODE = settings.RESUME_PARSER_SETTINGS['STRICT_MATCH_CODE']


class AccountResumeSavedSearchManager(object):
    """Менеджер для управления действиями и данными, которые
    не привязаны к активной на данный момент странице браузера"""
    settings = Configuration.get_solo()
    _key_words_for_adding = []
    _resume_pages = []

    def add_page_key_words(self, key_words):
        """Добавляет ключевые слова, находящихся на активной странице результатов."""
        self._key_words_for_adding.extend([x.lower() for x in key_words])

    @property
    def key_words_for_adding(self):
        """Нужно вызывать этот метод когда пройдены все страницы с сохраненными поисками.
        Возвращает все ключевые слова, которые есть в настройках, но нет в личном кабинете hh.ru"""
        return [x for x in list(set(self.settings.get_key_words) - set(self._key_words_for_adding)) if x]

    def create_saved_searches(self):
        """Создает сохраненные поиски в личном кабинете по ключевым словам из метода self.key_words_for_adding"""
        key_words_for_adding = self.key_words_for_adding
        logging.info('Creating saved searches for new keywords: \n{}'.format(key_words_for_adding))
        for key_word in key_words_for_adding:
            search_page = AccountSearchPage(key_word)
            search_page.open()
            search_page.save_search()

    def add_resume_pages(self, pages):
        """Добавляет страницу кандидата в общий список resume_pages"""
        self._resume_pages.extend(pages)

    @property
    def resume_pages(self):
        """Список страниц кандидатов с которых нужно скачать резюме"""
        return self._resume_pages


manager = AccountResumeSavedSearchManager()


class SettingsMixin(object):
    """Миксин для задания настроек"""
    settings = Configuration.get_solo()


class LoginPage(SettingsMixin):
    """Страница авторизации"""
    def open(self):
        """Окрывает станицу"""
        open_url('login')
        return self

    def login(self):
        """Авторизует пользователя"""
        logging.debug('Authenticating user with login: %s' % self.settings.login)
        s(by.xpath('//input[@data-qa="login-input-username"]')).set_value(self.settings.login)
        s(by.xpath('//input[@data-qa="login-input-password"]')).set_value(self.settings.password).press_enter()
        return AccountHomePage()


class AccountSearchPage(SettingsMixin):
    """Страница результатов поиска (для добавления новых ключевых слов)"""
    def __init__(self, key_word):
        self.key_word = key_word

    def open(self):
        """Производит поиск по указанному ключевому слову в указанном регионе"""
        open_url(
            'search/resume?text={key_word}&area={area}&clusters=true&'
            'exp_period=all_time&logic=normal&pos=full_text'.format(
                area=self.settings.search_area,
                key_word=self.key_word))
        return self

    @cached
    def is_valid(self, **kwargs):
        """Если по данному ключевому слову есть результаты поиска, то страница валидна"""
        msg = 'Page is {valid} for keyword "{keyword}"'
        try:
            valid = s(by.xpath('//div[@class="resumesearch__result"]'
                               '/span[@class="resumesearch__request"]')).text != '%s%s' % (QUERY_CODE, self.key_word)
            logging.debug(msg.format(valid='valid' if valid else 'NOT valid', keyword=self.key_word))
            return valid
        except:
            logging.debug(msg.format(valid='valid', keyword=self.key_word))
            return True

    def save_search(self):
        """Если страница валидна, создает сохраненный поиск"""
        if self.is_valid():
            logging.debug('Creating SavedSearch by keyword: "{keyword}"'.format(keyword=self.key_word))
            s(by.xpath('//span[@data-qa="resume-serp__save-search"]')).click()
        return self


class AccountHomePage(SettingsMixin):
    """Домашняя страница личного кабинета"""
    def get_resume_saved_search_pages(self):
        """Для удобства работы с пагинацией, создаются все страницы сохраненных поисков
        в диапазоне от 0 до saved_search_max_pages(задается в настройках в админке).
        Затем открывается каждая страница:
            Если страница существует, то обрабатывается дальше.
            Если возвращает 404 то значит, до нее была последняя страница;
            пробег по страницам останавливается."""
        pages_range = range(0, self.settings.saved_search_max_pages)
        return [AccountResumeSavedSearchListPage(x) for x in pages_range]


class AccountResumeSavedSearchListPage(SettingsMixin):
    """Страница с сохраненными поисками в ЛК"""
    # Если данный текст найден на странице, то значит больше страниц нет, дальше искать не нужно.
    PAGE_DOES_NOT_EXIST_TEXT = 'Такой страницы не существует'

    def __init__(self, page=0):
        self.page = page
        self.url = BASE_URL + 'resumesavedsearch?page=%d' % self.page

    def open(self):
        """Окрывает страницу с заданным номером"""
        open_url(self.url, is_full=True)
        return self

    @cached
    def is_valid(self, **kwargs):
        """Если странице не 404, то она валидна"""
        try:
            is_invalid = s(by.xpath(
                '//div[@class="bloko-columns-wrapper"]/div[@class="row-content"]')).text == self.PAGE_DOES_NOT_EXIST_TEXT
            logging.debug('{self} is {valid}'.format(valid='NOT valid' if is_invalid else 'valid', self=self))
            return not is_invalid
        except:
            logging.debug('{self} is {valid}'.format(valid='valid', self=self))
            return True

    @cached
    def get_saved_search_elements(self, **kwargs):
        """Возвращает все элементы SavedSearchElement, которые есть на данной странице"""
        if not self.is_valid():
            return []

        self.open()
        elements = ss(by.xpath('//div[@class="saved-search-item"]'))
        saved_search_elements = [SavedSearchElement(x) for x in elements]
        logging.debug('{self}: SavedSearchElements found:\n {elements}'.format(self=self,
                                                                               elements=saved_search_elements))
        return saved_search_elements

    def get_resume_search_results(self):
        """Возвращает все страницы с новыми резюме"""
        saved_search_elements = self.get_saved_search_elements()
        new_saved_search_elements = [page for page in [x.get_new_resume_list_page() for x in saved_search_elements] if
                                     page]
        logging.debug(
            '{self}: New resumes found'
            ' for SavedSearchElements:\n {elements}'.format(self=self,
                                                            elements=new_saved_search_elements))
        return new_saved_search_elements

    @cached
    def get_saved_search_key_words(self, **kwargs):
        """Возвращает список ключевых слов которые есть на данной странице"""
        msg = '{self}: keywords found: {keywords}"'

        key_words = ss(by.xpath('//div[@class="saved-search-item"]//h4[@data-qa="autosearch__title"]'))
        found_key_words = [x.text for x in key_words]

        logging.debug(msg.format(self=self, keywords=found_key_words))
        return found_key_words

    def delete_unneeded(self):
        """Удаляет сохраненный поиск, если данное ключевое слово было удалено в настройках.
        После каждого удаления проверяет данную страницу заново, тк при удалении, происходит сдвиг
        элементов на станице из следующей страницы. Удалени завершается, только когда на данной странице
        не остается не одного сохраненного поиска подлежащего удалению."""
        for_delete = [x for x in self.get_saved_search_elements(refresh=True) if x.should_be_deleted]
        while for_delete:
            for_delete_item = for_delete[0]
            for_delete_item.check_for_delete()

            # Необходимо проскроллить наверх страницы, чтобы плавающее меню не перекрывало кнопку "Удалить"
            logging.debug('{} scrolling to top of the page'.format(self))
            for_delete_item.element._webdriver.execute_script("window.scrollTo({x},{y});".format(x=0, y=0))

            logging.debug('{} deleting saved search'.format(for_delete_item))
            s(by.xpath('//button[@class="bloko-button HH-SavedSearch-DeleteButton"]')).click()
            for_delete = [x for x in self.get_saved_search_elements(refresh=True) if x.should_be_deleted]

    def __repr__(self):
        return '<{}: {}>'.format(self.__class__.__name__, self.url)

    def __str__(self):
        return self.__repr__()


class SavedSearchElement(SettingsMixin):
    """Элемент сохраненного поиска на станице списка сохраненных поисков"""
    def __init__(self, element):
        self.element = element

    @property
    @cached
    def title(self):
        """Название сохраненного поиска (одновременно ключевое слово по которому был создан
        данный сохраненный поиск)"""
        return self.element.s(by.xpath('.//h4[@data-qa="autosearch__title"]'))

    @property
    def new_resumes(self):
        """Элемент, указывающий, что у данного сохраненного поиска есть новые резюме"""
        return self.element.s(by.xpath('.//div[@class="saved-search__links"]'
                                       '/a[@data-qa="autosearch__results-counter_new"]'))

    @property
    def has_new(self):
        """Флаг, указывающий, есть у данного сохраненного поиска, новые резюме"""
        try:
            self.new_resumes.should(exist)
            logging.debug('{} has new resumes'.format(self))
            return True
        except TimeoutException:
            logging.debug('{} does NOT have new resumes'.format(self))
            return False

    def get_new_resume_list_page(self):
        """Если есть новые резюме, то возвращает страницу со списком новых резюме"""
        if self.has_new:
            url = self.new_resumes.get_attribute('href')
            return ResumeListPage(url)

    def check_for_delete(self):
        """Удаляет данный сохраненный поиск"""
        logging.debug('{} selecting checkbox for deleting'.format(self))
        self.element.s(by.xpath('.//div[@class="saved-search-item-checkbox"]')).s(
            by.xpath('.//label')).press_enter().send_keys(Keys.SPACE)

    @property
    @cached
    def should_be_deleted(self):
        """Проверяет по ключевым словам из настроек, должен ли быть удален данный сохраненный поиск"""
        for_deleting = self.title.text.lower() not in self.settings.get_key_words
        if for_deleting:
            logging.debug('{} should be deleted'.format(self))
        return for_deleting

    def __repr__(self):
        return '<{}: {}>'.format(self.__class__.__name__, self.title.text)

    def __str__(self):
        return self.__repr__()


class ResumeListPage(SettingsMixin):
    """Страница со списком резюме"""
    def __init__(self, url):
        self.url = url

    def open(self):
        open_url(self.url, is_full=True)
        return self

    @cached
    def get_search_result_elements(self, **kwargs):
        """Список объектов ResumeSearchResultElement на данной странице"""
        elements = ss(by.xpath('//div[@data-qa="resume-serp__resume"]'))
        return [ResumeSearchResultElement(x) for x in elements]

    @cached
    def get_resume_pages(self):
        """Возвращает список страниц с резюме которые есть на данной странице"""
        resume_pages = [x.get_resume_page() for x in self.get_search_result_elements()]
        logging.debug('{self} ResumePages found:\n {resume_pages}'.format(self=self, resume_pages=resume_pages))
        return resume_pages

    def __repr__(self):
        return '<{}: {}>'.format(self.__class__.__name__, self.url)

    def __str__(self):
        return self.__repr__()


class ResumeSearchResultElement(SettingsMixin):
    """Элемент с информацией о резюме на странице списка резюме"""
    def __init__(self, element):
        self.element = element

    @cached
    def get_resume_page(self, **kwargs):
        """Возвращает страницу резюме, которая соответсвует данному элементу"""
        url = self.element.s(by.xpath('.//a[contains(@data-qa, "resume-serp__resume-title")]')).get_attribute('href')
        return ResumePage(url)


class ResumePage(SettingsMixin):
    """Страница кандидата и резюме"""
    download_to = settings.RESUME_PARSER_SETTINGS['FILES_DOWNLOAD_PATH']
    download_timeout = settings.RESUME_PARSER_SETTINGS['FILE_DOWNLOAD_TIMEOUT_SECONDS']
    initial_files_count = None

    def __init__(self, url):
        self.url = url

    def open(self):
        open_url(self.url, is_full=True)
        return self

    def download_pdf_resume(self):
        """Скачивает резюме в формате PDF
        Перед вызовом данного метода нужно выполнить self.set_initial_files_count()"""
        s(by.xpath('//div[@class="HH-Resume-DownloadDropdown"]'
                   '//li[contains(@class, "list-params__item_download-adobereader")]')).click()
        self.wait_and_rename_file('pdf')

    def download_resume(self):
        """Скачивает резюме"""
        self.set_initial_files_count()
        s(by.xpath('//div[contains(@class, "resume-header-actions__buttons-wrapper")]//button')).click()
        self.download_pdf_resume()

    def set_initial_files_count(self):
        """Указывает сколько файлов было в директории до начала скачивания"""
        path = self.download_to
        if not os.path.exists(path):
            os.makedirs(path)
        self.initial_files_count = len([f for f in os.listdir(path)])

    def file_ext_is_valid(self, file_name, ext):
        """Проверяет расширение файла по имени, если соответсвует ext, то True"""
        if not file_name or len(file_name.split('.')) < 2:
            return False
        return file_name.split('.')[-1] == ext

    def get_file_name(self, dir_files, path):
        if dir_files:
            try:
                return max(dir_files, key=lambda xa: os.path.getctime(os.path.join(path, xa)))
            except FileNotFoundError:
                pass
        return None

    def wait_and_rename_file(self, expected_ext):
        """Селениум не умеет дожидаться полной закачки файла.
        Чтобы файлы корректно скачивались:
            Фиксируем сколько файлов было в директории до клика на скачать
            Каждую секунду проверяем:
                Увеличилось ли количество файлов в директории на 1
                Соответсвует ли расширение нового файла ожидаемому
                Когда данные условия удавлетворены, то считаем что файл скачан.
                При необходимости можно закрыть браузер.

                Данный способ косвенно подтверждает, что файл скачан, тк браузер
                создает реальное расширение файла, только тогда когда он полностью
                скачан и работа с ним завершена.

                self.download_to - куда будет скачан файл
                self.download_timeout - максимальное время на скачивание файла
                """
        path = self.download_to
        dir_files = os.listdir(path)
        file_name = self.get_file_name(dir_files, path)
        time_start = time.time()
        success = True

        while (self.initial_files_count >= len(dir_files)) or not self.file_ext_is_valid(file_name=file_name,
                                                                                         ext=expected_ext):
            time.sleep(1)

            dir_files = os.listdir(path)
            file_name = self.get_file_name(dir_files, path)

            if time.time() - time_start > self.download_timeout:
                success = False
                break

        if success:
            logging.info('{self}: download COMPLETED successfully: file {file}'.format(self=self, file=file_name))
        else:
            logging.info('{self}: download FAILED: file {file}'.format(self=self, file=file_name))

    def __repr__(self):
        return '<{}: {}>'.format(self.__class__.__name__, self.url)

    def __str__(self):
        return self.__repr__()
