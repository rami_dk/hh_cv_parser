import os

from django.conf import settings

from selenium.common.exceptions import TimeoutException

from apps.resume_parser.resume_parser_app.page_objects import LoginPage, manager
from PyPDF2 import PdfFileMerger


def download_resumes():
    """Производит синхронизацию ключевых слов и скачивание резюме"""
    login_page = LoginPage()
    login_page.open()
    account_home_page = login_page.login()
    saved_search_pages = account_home_page.get_resume_saved_search_pages()
    for page in saved_search_pages:
        page = page.open()
        if not page.is_valid():
            break

        page.delete_unneeded()
        manager.add_page_key_words(page.get_saved_search_key_words())

        search_result_pages = page.get_resume_search_results()
        for search_result_page in search_result_pages:
            search_result_page.open()
            manager.add_resume_pages(search_result_page.get_resume_pages())

    for resume_page in manager.resume_pages:
        resume_page.open()

        # Иногда скачивание файла не срабатывает, поэтому нужно перезагрузить
        # страницу и попробовать еще раз.
        for i in range(settings.RESUME_PARSER_SETTINGS['FILE_DOWNLOAD_TRIES']):
            try:
                resume_page.download_resume()
            except TimeoutException:
                resume_page.open()
            else:
                # Если скачивание прошло без ошибок, то выходим из цикла.
                break

    manager.create_saved_searches()


def merge_resumes(base_dir, files_dir, result_dir, result_file_name):
    """Объединяет все файлы резюме из from_dir в один и возвращает его с именем result_file_name"""
    from_dir = os.path.join(base_dir, files_dir)
    return merge_pdfs(from_dir, result_dir, result_file_name)


def merge_pdfs(from_dir, result_dir, result_file_name):
    """Объединяет все PDF файлы из from_dir в один и возвращает его с именем result_file_name"""
    file_names = os.listdir(from_dir)
    merger = PdfFileMerger()

    [merger.append(os.path.join(from_dir, x)) for x in file_names]

    if not os.path.exists(result_dir):
        os.makedirs(result_dir)

    result_file = os.path.join(result_dir, '%s.pdf' % result_file_name)
    merger.write(result_file)

    return result_file
