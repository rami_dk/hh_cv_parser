import functools


def cached(function):
    """Позволяет закешировать данные, сохранив в приватную переменную
    для инвалидации, необходимо передать 'refresh'=True."""
    @functools.wraps(function)
    def wrapper(*args, **kwargs):
        private_attr = '__{}'.format(function.__name__)
        self_ = args[0]

        if not hasattr(self_, private_attr):
            setattr(self_, private_attr, None)

        if kwargs.get('refresh', False) or getattr(self_, private_attr) is None:
            result = function(*args, **kwargs)
            setattr(self_, private_attr, result)

        return getattr(self_, private_attr)
    return wrapper