import logging

from selenium.webdriver.remote.webdriver import WebDriver

import selene.config
import selene.driver
import selene.factory
from .factory import ensure_driver_started


def driver():
    # type: () -> WebDriver
    return ensure_driver_started(selene.config.browser_name)


def open_url(absolute_or_relative_url, is_full=False):
    """
    Loads a web page in the current browser session.
    :param absolgenerateute_or_relative_url:
        an absolute url to web page in case of config.base_url is not specified,
        otherwise - relative url correspondingly

    :Usage:
        open_url('http://mydomain.com/subpage1')
        open_url('http://mydomain.com/subpage2')
        # OR
        config.base_url = 'http://mydomain.com'
        open_url('/subpage1')
        open_url('/subpage2')
    """
    # todo: refactor next line when app_host is removed
    if is_full:
        logging.debug('Opening url: %s' % absolute_or_relative_url)
        driver().get(absolute_or_relative_url)
    else:
        base_url = selene.config.app_host if selene.config.app_host else selene.config.base_url
        logging.debug('Opening url: %s' % (base_url + absolute_or_relative_url))
        driver().get(base_url + absolute_or_relative_url)