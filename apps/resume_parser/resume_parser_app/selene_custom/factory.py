import atexit

from django.conf import settings
from selenium import webdriver
from selenium.common.exceptions import UnexpectedAlertPresentException
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager
from selenium.webdriver.remote.webdriver import WebDriver

import selene
import selene.driver
from selene import config
from selene.browsers import BrowserName


def set_shared_driver(driver):
    selene.driver._shared_web_driver_source.driver = driver
    config.browser_name = driver.name


def get_shared_driver():
    return selene.driver._shared_web_driver_source.driver


def is_another_driver(driver):
    try:
        return get_shared_driver().session_id != driver.session_id
    except AttributeError:
        return False


def is_driver_still_open(webdriver):
    # type: (WebDriver) -> bool
    try:
        webdriver.title
    # todo: specify exception?.. (unfortunately there Selenium does not use some specific exception for this...)
    except UnexpectedAlertPresentException:
        return True
    except Exception:
        return False
    return True


def driver_has_started(name):
    shared_driver = get_shared_driver()
    if not shared_driver:
        return False
    return shared_driver.name == name \
           and shared_driver.session_id \
           and is_driver_still_open(shared_driver)


def kill_all_started_drivers():
    atexit._run_exitfuncs()


def ensure_driver_started(name):
    if driver_has_started(name):
        return get_shared_driver()

    return _start_driver(name)


def __start_chrome():
    options = webdriver.ChromeOptions()
    if settings.RESUME_PARSER_SETTINGS['CHROME_HEADLESS']:
        options.add_argument("--headless")
    options.add_argument('--no-sandbox')
    prefs = {
        'download.default_directory': settings.RESUME_PARSER_SETTINGS['FILES_DOWNLOAD_PATH'],
        'download.prompt_for_download': False,
        'download.directory_upgrade': True,
        'safebrowsing.enabled': False,
        # 'plugins.always_open_pdf_externally': True,
        'safebrowsing.disable_download_protection': True
    }

    options.add_experimental_option('prefs', prefs)

    if config.start_maximized:
        options.add_argument("--start-maximized")
    driver = webdriver.Chrome(executable_path=ChromeDriverManager().install(),
                            chrome_options=options,
                            desired_capabilities=config.desired_capabilities)

    enable_download_in_headless_chrome(driver, settings.RESUME_PARSER_SETTINGS['FILES_DOWNLOAD_PATH'])
    return driver


def enable_download_in_headless_chrome(driver, download_dir):
    """
    there is currently a "feature" in chrome where
    headless does not allow file download: https://bugs.chromium.org/p/chromium/issues/detail?id=696481
    This method is a hacky work-around until the official chromedriver support for this.
    Requires chrome version 62.0.3196.0 or above.
    """

    # add missing support for chrome "send_command"  to selenium webdriver
    driver.command_executor._commands["send_command"] = ("POST", '/session/$sessionId/chromium/send_command')

    params = {'cmd': 'Page.setDownloadBehavior', 'params': {'behavior': 'allow', 'downloadPath': download_dir}}
    driver.execute("send_command", params)


def __start_firefox(name):
    executable_path = "wires"
    if name == BrowserName.MARIONETTE:
        executable_path = GeckoDriverManager().install()
    driver = webdriver.Firefox(capabilities=config.desired_capabilities,
                               executable_path=executable_path)
    if config.start_maximized:
        driver.maximize_window()
    return driver


def __get_driver(name):
    if name == BrowserName.CHROME:
        return __start_chrome()
    else:
        return __start_firefox(name)


def _start_driver(name):
    kill_all_started_drivers()
    driver = __get_driver(name)
    set_shared_driver(driver)
    if not config.hold_browser_open:
        _register_driver(driver)
    return driver


def _register_driver(driver):
    atexit.register(driver.quit)
