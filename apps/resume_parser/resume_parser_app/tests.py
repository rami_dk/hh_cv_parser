import os
import shutil
from django.conf import settings
from django.test import TestCase

# Create your tests here.
from selenium.common.exceptions import TimeoutException

from .page_objects import LoginPage, ResumePage


class TestDownload(TestCase):
    def setUp(self):
        self.download_path = settings.RESUME_PARSER_SETTINGS['FILES_DOWNLOAD_PATH']
        shutil.rmtree(self.download_path, ignore_errors=True)

        if not os.path.exists(self.download_path):
            os.makedirs(self.download_path)

        login_page = LoginPage()
        login_page.open()
        login_page.login()

    def test_download_chrome_headless(self):
        self.assertFalse(bool(os.listdir(self.download_path)))

        resume_page = ResumePage('https://ufa.hh.ru/resume/bfbbcc75000209458e000012a9665a6976355a?query=horiba')
        resume_page.open()
        # Иногда скачивание файла не срабатывает, поэтому нужно перезагрузить
        # страницу и попробовать еще раз.
        for i in range(settings.RESUME_PARSER_SETTINGS['FILE_DOWNLOAD_TRIES']):
            try:
                resume_page.download_resume()
            except TimeoutException:
                resume_page.open()
            else:
                # Если скачивание прошло без ошибок, то выходим из цикла.
                break

        self.assertTrue(bool(os.listdir(self.download_path)))

    def tearDown(self):
        shutil.rmtree(self.download_path, ignore_errors=True)
