import datetime
import uuid

from django.conf import settings
from django.urls import reverse

from apps.response_backends import BaseResponse, BaseDebugResponse
from apps.resume_parser.resume_parser_app.models import Configuration
from apps.resume_parser.resume_parser_app.page_objects.utils import merge_resumes


class BaseParserResponse(BaseResponse):
    """Базовый класс ответа парсинга резюме"""
    settings = Configuration.get_solo()

class ResumeResponse(BaseParserResponse):
    def __init__(self):
        self.file_uuid = uuid.uuid4()
        self.response_data = self.get_response_data()
        super().__init__()

    def get_response_data(self):
        resume_parser_settings = settings.RESUME_PARSER_SETTINGS

        resumes_for_date = datetime.datetime.now().strftime('%d_%m_%Y')
        result_file = merge_resumes(base_dir=resume_parser_settings['FILES_DOWNLOAD_PATH_BASE'],
                                    files_dir=resumes_for_date,
                                    result_dir=resume_parser_settings['MERGED_FILES_PATH'],
                                    result_file_name='%s_%s_result' % (self.file_uuid, resumes_for_date))
        data = {
            'result_file': result_file
        }
        return data

    def text(self):
        text = 'Подборка новых резюме доступна по ссылке: %s' % (
                settings.DOMAIN_NAME + reverse('resume_parser:merged_resumes', kwargs={'file_uuid': self.file_uuid}))
        return text

    @property
    def attachment(self):
        return []

    @property
    def subject(self):
        now = datetime.datetime.now()
        return 'Подборка резюме на %s' % now.date().strftime('%d.%m.%Y')


class ErrorResponse(ResumeResponse):
    """Уведомление пользователя, о том что проишел сбой при работе бота.
    Данное ответ - только информационный, и должен быть отправлен получателям ResumeResponse"""
    def get_response_data(self):
        pass

    @property
    def attachment(self):
        return []

    def text(self):
        text = 'Мне не удалось собрать все или часть нужных данных для сравнения. ' \
               'Я уже сообщил о своей проблеме моему разработчику. Он поможем восстановить мою работоспособность.'
        return text


class DebugResponse(BaseParserResponse, BaseDebugResponse):
    @property
    def subject(self):
        now = datetime.datetime.now()
        return '[СБОЙ] Подборка резюме на %s' % now.date().strftime('%d.%m.%Y')



